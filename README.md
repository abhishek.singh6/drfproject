# DRF Project (IPL APIs)
---

#### Dependencies
1. Python (programming language)
2. Django (Django is an open source web application framework written in python)
3. Django Rest Framework(DRF) (Framework for creating REST APIs)

IPL Dataset is taken from [kaggle.com](https://www.kaggle.com/manasgarg/ipl) that have Ball by ball 
details for all matches for all season.

### Problem Set
***
```
Create various APIs for IPL dataset using Django Rest Framework(DRF)
```












